import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import Main from '@/components/Main';

import Cave from '@/components/content/Cave';
import ProfilePage from '@/components/content/cave/Profile';
import UserInfo from '@/components/content/cave/profile/UserInfo';
import UserGuestbook from '@/components/content/cave/profile/UserGuestbook';

// import ApbergetWeather from '@/components/ApbergetWeather'

Vue.use(Router)

export default new Router({

	// mode: 'history',

	routes: [
		
		/*
		{
			path: '/',
			name: 'HelloWorld',
			component: HelloWorld
		},
		*/

		{
			path: '/main',
			name: 'Main',
			component: Main,

			children: [
				{
					path: 'cave',
					component: Cave,

					redirect: '/main/cave/profile',
					
					meta: {
						submenu: [
							{ url: '/main/cave/profile', 	text: 'Din profil' },
							{ url: '/main/cave/supporter', 	text: 'Supporter'},
							{ url: '/main/cave/friendwhat', text: 'Vänbevakning'},
							{ url: '/main/cave/visitors', 	text: 'Besökslogg'},
							{ url: '/main/cave/messages', 	text: 'Meddelanden'},
							{ url: '/main/cave/settings', 	text: 'Inställningar'}
						]
					},
					
					children: [
						{
							path: 'profile',

							component: ProfilePage,

							redirect: '/main/cave/profile/userinfo',

							meta: {
								tabs: [
									{ url: '/main/cave/profile/userinfo', 	text: 'Användarinfo' },
									{ url: '/main/cave/profile/guestbook', 	text: 'Gästbok' },
									{ url: '/main/cave/profile/diary', 		text: 'Diarium' },
									{ url: '/main/cave/profile/albums', 	text: 'Fotoalbum' },
									{ url: '/main/cave/profile/links', 		text: 'Länkar' },
									{ url: '/main/cave/profile/friends', 	text: 'Vänner' },
									{ url: '/main/cave/profile/things', 	text: 'Saker' }
								]
							},

							children: [
								{
									path: 'userinfo',
									component: UserInfo
								},
								{
									path: 'guestbook',
									component: UserGuestbook
								}
							]							
							
						}
					]
				},
				{
					path: 'current',
					component: Cave,
					meta: {
						submenu: [
							{ url: '/main/current/news', 		text: 'Nyheter' },
							{ url: '/main/current/invites', 	text: 'Inbjudningar'},
							{ url: '/main/current/contests', 	text: 'Tävlingar'},
							{ url: '/main/current/screen', 		text: 'Skärmen'},
							{ url: '/main/current/chat', 		text: 'Chattintervjuer'}
						]
					},

				}
			]
		},

	]

});
